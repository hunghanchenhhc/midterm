package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;
// Hung Han Chen
public class CelsiusTest {

	@Test
	public void testFromFahrenheitRegular() {
		double celsius = Celsius.fromFahrenheit(32);
		assertTrue("Fahrenheit provide not match result", celsius == 0);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testFromFahrenheitExceptional() {
		double celsius = Celsius.fromFahrenheit(-20);
		assertTrue("Fahrenheit provide not match result", celsius == 25);
	}
	
	@Test
	public void testFromFahrenheitBoundaryIn() {
		double celsius = Celsius.fromFahrenheit(70);
		assertTrue("Fahrenheit provide not match result", celsius == 21);
	}
	
	@Test
	public void testFromFahrenheitBoundaryOut() {
		double celsius1 = Celsius.fromFahrenheit(70);
		double celsius2 = Celsius.fromFahrenheit(71);
		System.out.println(celsius1);
		System.out.println(celsius2);

		assertFalse("Fahrenheit provide not match result", celsius1 != celsius2);
	}
}
